# Safetyd
This daemon serves as a proxy for the safety loop.  It closes the safety loop and keeps it closed so long as it recieves keep-alive messages over UDP.  If it stops recieving keep-alive messages it opens the safety loop.  It can also recieve an explicit open message to open the safety loop.  Messages are 1 byte in size.

A 1 is a "keep-alive and close" message.
A 0 is an "open immediately" message.

