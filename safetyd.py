#!/usr/bin/env python3
# Copyright 2016 Brendon Carroll

import asyncio
import socket

import RPi.GPIO as GPIO

PORT = 6666
SAFETY_LOOP_PIN = 7
TIMOUT_PERIOD = 0.200

class Watchdog(object):
    '''A simple watchdog timer for use with asyncio.
    '''

    def __init__(self, period, callback):
        self.period = period
        self.callback = callback
        self.task = asyncio.ensure_future(self.expire())

    async def expire(self):
        try:
            await asyncio.sleep(self.period)
            print('Watchdog expired.')
            self.callback()
        except asyncio.CancelledError:
            pass

    def reset(self):
        '''Resets the timer.'''
        self.task.cancel()
        self.task = asyncio.ensure_future(self.expire())

class SafetyLoop(object):

    def __init__(self, pin):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(pin, GPIO.OUT)
        # Set safety loop to open
        GPIO.output(SAFETY_LOOP_PIN, False)
        self.wt = Watchdog(TIMOUT_PERIOD, self.open)

    def open(self):
        GPIO.output(SAFETY_LOOP_PIN, False)

    def close(self):
        GPIO.output(SAFETY_LOOP_PIN, True)
        self.wt.reset()

async def main():
    safety_loop = SafetyLoop(SAFETY_LOOP_PIN)

    loop = asyncio.get_event_loop()
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM,)
    s.setblocking(False)
    s.bind(('127.0.0.1', PORT))

    while True:
        data = await loop.sock_recv(s, 1)
        print(data)
        if data == b'\x00':
            print('Opening safety loop')
            safety_loop.open()
        elif data == b'\x01':
            print('Closing safety loop')
            safety_loop.close()


if __name__ == '__main__':
    l = asyncio.get_event_loop()
    l.run_until_complete(main())
